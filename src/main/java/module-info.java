module com.example.labyrinthgraphique {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.labyrinthgraphique to javafx.fxml;
    opens com.example.labyrinthgraphique.controller;
    exports com.example.labyrinthgraphique;
    exports com.example.labyrinthgraphique.controller;

}