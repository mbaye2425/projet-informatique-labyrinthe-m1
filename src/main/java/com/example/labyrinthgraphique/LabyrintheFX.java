package com.example.labyrinthgraphique;

import com.example.labyrinthgraphique.controller.LabyrintheController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.io.IOException;

public class LabyrintheFX extends Application {
    public void start(Stage stage) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("labyrinthe.fxml"));
        Parent root = loader.load();

        LabyrintheController controller = loader.getController();
        controller.setStage(stage);

        Scene scene = new Scene(root);
        scene.setOnKeyPressed(controller::handleKeyPress);
        stage.setTitle("Labyrinthe App");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}
