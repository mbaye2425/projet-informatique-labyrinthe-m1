package com.example.labyrinthgraphique.controller;

import com.example.labyrinthgraphique.model.Labyrinthe;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.util.List;

public class LabyrintheController {

    int cellSize;
    int taille;

    @FXML
    private Canvas labyrintheCanvas;

    @FXML
    private TextField tailleTextField;
    private Labyrinthe labyrinthe;
    private int positionX;
    private int positionY;
    private Stage stage;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    public void genererLabyrinthe() {
        try {
            taille = Integer.parseInt(tailleTextField.getText());
            labyrinthe = new Labyrinthe(taille);
            labyrinthe.generer();
            cellSize = Math.max(10, (int) labyrintheCanvas.getWidth() / taille);
            labyrintheCanvas.setWidth(taille * cellSize);
            labyrintheCanvas.setHeight(taille * cellSize);
            int entreeX = 0;
            int entreeY = labyrinthe.getEntree();
            positionX = entreeX;
            positionY = entreeY;
            drawLabyrinthe();

        } catch (NumberFormatException e) {
            System.out.println("Veuillez entrer un nombre entier valide pour la taille.");
        }
    }

    public void regenererLabyrinthe(int taille){
        labyrinthe = new Labyrinthe(taille);
        labyrinthe.generer();
        cellSize = Math.max(10, (int) labyrintheCanvas.getWidth() / taille);
        labyrintheCanvas.setWidth(taille * cellSize);
        labyrintheCanvas.setHeight(taille * cellSize);
        int entreeX = 0;
        int entreeY = labyrinthe.getEntree();
        System.out.println(entreeX + "" +entreeY);
        positionX = entreeX;
        positionY = entreeY;
        drawLabyrinthe();
    }




    private void drawLabyrinthe() {
        GraphicsContext gc = labyrintheCanvas.getGraphicsContext2D();
        gc.clearRect(0, 0, labyrintheCanvas.getWidth(), labyrintheCanvas.getHeight());
        int taille = labyrinthe.getTaille();
        int cellSize = Math.max(10, (int) labyrintheCanvas.getWidth() / taille);

        System.out.println("Cell size: " + cellSize);
        System.out.println("Canvas width: " + labyrintheCanvas.getWidth());
        System.out.println("Canvas height: " + labyrintheCanvas.getHeight());

        for (int i = 0; i < labyrinthe.getTaille(); i++) {
            for (int j = 0; j < labyrinthe.getTaille(); j++) {
                int x = j * cellSize;
                int y = i * cellSize;

                System.out.println("Drawing cell at: " + i + ", " + j);

                if (labyrinthe.estMurHaut(i, j)) {
                    gc.strokeLine(x, y, x + cellSize, y);
                }
                if (labyrinthe.estMurGauche(i, j)) {
                    gc.strokeLine(x, y, x, y + cellSize);
                }
                if (labyrinthe.estMurDroit(i, j)) {
                    gc.strokeLine(x + cellSize, y, x + cellSize, y + cellSize);
                }
                if (labyrinthe.estMurBas(i, j)) {
                    gc.strokeLine(x, y + cellSize, x + cellSize, y + cellSize);
                }
            }
        }
        gc.setFill(Color.BLUE);
        //drawBonhomme(positionX * cellSize + cellSize / 2, positionY * cellSize + cellSize / 2, cellSize);
        gc.fillOval(positionX * cellSize, positionY * cellSize, cellSize/1.5, cellSize/1.5);
        gc.setStroke(Color.BLACK);

    }


    @FXML
    public void calculerSolution() {
    }

    @FXML
    public void calculerPlusCourtChemin() {
        List<Labyrinthe.Cellule> chemin = labyrinthe.calculerPlusCourtChemin();
        System.out.println(chemin.iterator());
        drawChemin(chemin);
    }


    private void drawChemin(List<Labyrinthe.Cellule> chemin) {
        GraphicsContext gc = labyrintheCanvas.getGraphicsContext2D();
        gc.setStroke(Color.GREEN);
        gc.setLineWidth(2);

        for (int i = 0; i < chemin.size() - 1; i++) {
            Labyrinthe.Cellule current = chemin.get(i);
            Labyrinthe.Cellule next = chemin.get(i + 1);

            int x1 = current.y * cellSize + cellSize / 2;
            int y1 = current.x * cellSize + cellSize / 2;
            int x2 = next.y * cellSize + cellSize / 2;
            int y2 = next.x * cellSize + cellSize / 2;

            gc.strokeLine(x1, y1, x2, y2);
        }
        this.activerModeJeu();
    }



    @FXML
    public void activerModeJeu() {
        labyrintheCanvas.setFocusTraversable(true);
        labyrintheCanvas.setOnKeyPressed(this::handleKeyPress);
        labyrintheCanvas.requestFocus();
    }

    public void handleKeyPress(KeyEvent event) {

        event.consume();
        int nouvellePositionX = positionX;
        int nouvellePositionY = positionY;

        switch (event.getCode()) {
            case UP:
                // Déplacement vers le haut, vérifier le mur en haut
                if (nouvellePositionY > 0 && !labyrinthe.estMurHaut(nouvellePositionY , nouvellePositionX)) {
                    nouvellePositionY--;

                }
                break;
            case DOWN:
                // Déplacement vers le bas, vérifier le mur en bas
                if (nouvellePositionY < labyrinthe.getTaille() - 1 && !labyrinthe.estMurBas(nouvellePositionY, nouvellePositionX)) {
                    nouvellePositionY++;
                }
                break;
            case LEFT:
                // Déplacement vers la gauche, vérifier le mur à gauche
                if (nouvellePositionX > 0 && !labyrinthe.estMurGauche(nouvellePositionY, nouvellePositionX)) {
                    nouvellePositionX--;
                }
                break;
            case RIGHT:
                // Déplacement vers la droite, vérifier le mur à droite
                if (nouvellePositionX <= labyrinthe.getTaille()-1 && !labyrinthe.estMurDroit(nouvellePositionY, nouvellePositionX)) {
                    if(nouvellePositionX == labyrinthe.getTaille()-1 && nouvellePositionY == labyrinthe.getSortie() && !labyrinthe.estMurDroit(nouvellePositionY,nouvellePositionX)){
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Félicitation");
                        alert.setHeaderText(null);
                        alert.setContentText("Bravo vous avez gagné!");
                        alert.showAndWait().filter(response -> response == ButtonType.OK).ifPresent(response -> {
                            this.regenererLabyrinthe(Integer.parseInt(tailleTextField.getText())+1);
                            int newTaille = this.getTaille()+1;
                            tailleTextField.setText(String.valueOf(Integer.parseInt(tailleTextField.getText())+1));

                        });
                        return;
                    }
                    nouvellePositionX++;
                }
                break;
            default:
                return;
        }


        System.out.println("Position actuelle: (" + positionY + ", " + positionX + ")");
        System.out.println("Nouvelle position proposée: (" + nouvellePositionY + ", " + nouvellePositionX + ")");
        System.out.println("Murs - Haut: " + labyrinthe.estMurHaut(positionY, positionX) +
                ", Bas: " + labyrinthe.estMurBas(positionY, positionX) +
                ", Gauche: " + labyrinthe.estMurGauche(positionY, positionX) +
                ", Droit: " + labyrinthe.estMurDroit(positionY, positionX));


        if (nouvellePositionX != positionX || nouvellePositionY != positionY) {
            positionX = nouvellePositionX;
            positionY = nouvellePositionY;
            drawLabyrinthe();
            labyrintheCanvas.requestFocus();
        }
    }

    @FXML
    private void initialize() {
        tailleTextField.setText("20");
        Platform.runLater(this::genererLabyrinthe);
    }

    public int getPositionX() {
        return positionX;
    }

    public int getPositionY() {
        return positionY;
    }

    public int getTaille() {
        return taille;
    }

    private void drawBonhomme(int x, int y, int size) {
        GraphicsContext gc = labyrintheCanvas.getGraphicsContext2D();
        int radius = size / 4; // Taille de la tête
        int bodyHeight = size / 2; // Hauteur du corps
        int limbLength = size / 3; // Longueur des membres

        // Dessiner la tête
        gc.strokeOval(x - radius, y - size, radius * 2, radius * 2);

        // Dessiner le corps
        gc.strokeLine(x, y, x, y + bodyHeight);

        // Dessiner les bras
        gc.strokeLine(x, y + bodyHeight / 3, x - limbLength, y + bodyHeight / 3);
        gc.strokeLine(x, y + bodyHeight / 3, x + limbLength, y + bodyHeight / 3);

        // Dessiner les jambes
        gc.strokeLine(x, y + bodyHeight, x - limbLength, y + size);
        gc.strokeLine(x, y + bodyHeight, x + limbLength, y + size);
    }

}
