package com.example.labyrinthgraphique.model;
import java.util.*;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;


public class Labyrinthe {
    public int getTaille() {
        return taille;
    }

    public int getEntree() {
        return entree;
    }

    public int getSortie() {
        return this.sortie;
    }


    public class Cellule {
        boolean murHaut = true, murDroit = true, murBas = true, murGauche = true;
        int composante;
        public int x ;
        public int y ;

        // Informations pour le calcul du chemin
        int coutActuel = Integer.MAX_VALUE;
        Cellule precedente = null;

        Cellule(int comp,int x , int y ) {

            this.composante = comp;
            this.x = x;
            this.y = y;

        }
    }

    private int taille;
    private Cellule[][] grille;
    private Random rand = new Random();

    public Labyrinthe(int taille) {
        this.taille = taille;
        this.grille = new Cellule[taille][taille];
        int comp = 0;
        for (int i = 0; i < taille; i++) {
            for (int j = 0; j < taille; j++) {
                grille[i][j] = new Cellule(comp,i,j);
                comp++;
            }
        }
    }

    public boolean estMurHaut(int y, int x) {
        return grille[y][x].murHaut;
    }

    public boolean estMurBas(int y, int x) {
        return grille[y][x].murBas;
    }

    public boolean estMurGauche(int y, int x) {
        return grille[y][x].murGauche;
    }

    public boolean estMurDroit(int y, int x) {
        return grille[y][x].murDroit;
    }


    public Cellule getEntreeCellule() {
        return grille[this.entree][0];
    }

    public void fusionner(int x1, int y1, int x2, int y2) {
        Cellule c1 = grille[x1][y1];
        Cellule c2 = grille[x2][y2];

        int ancienneComp = c2.composante;
        int nouvelleComp = c1.composante;

        for (int i = 0; i < taille; i++) {
            for (int j = 0; j < taille; j++) {
                if (grille[i][j].composante == ancienneComp) {
                    grille[i][j].composante = nouvelleComp;
                }
            }
        }

        // Brise le mur en fonction de la position relative de c1 et c2
        if (x1 == x2) {
            if (y1 < y2) {
                c1.murDroit = false;
                c2.murGauche = false;
            } else {
                c1.murGauche = false;
                c2.murDroit = false;
            }
        } else {
            if (x1 < x2) {
                c1.murBas = false;
                c2.murHaut = false;
            } else {
                c1.murHaut = false;
                c2.murBas = false;
            }
        }
    }


    private int entree, sortie;
    public void generer() {

        entree = rand.nextInt(taille);
        sortie = rand.nextInt(taille);

        grille[entree][0].murGauche = false;
        grille[sortie][taille - 1].murDroit = false;

        while (true) {
            int i = rand.nextInt(taille);
            int j = rand.nextInt(taille);

            List<int[]> voisins = new ArrayList<>();

            if (i > 0) voisins.add(new int[]{i-1, j});
            if (i < taille - 1) voisins.add(new int[]{i+1, j});
            if (j > 0) voisins.add(new int[]{i, j-1});
            if (j < taille - 1) voisins.add(new int[]{i, j+1});

            if (voisins.size() == 0) continue;

            int[] coord = voisins.get(rand.nextInt(voisins.size()));

            Cellule c1 = grille[i][j];
            Cellule c2 = grille[coord[0]][coord[1]];

            if (c1.composante != c2.composante) {
                fusionner(i, j, coord[0], coord[1]);
            }

            boolean termine = true;
            for (int x = 0; x < taille && termine; x++) {
                for (int y = 0; y < taille && termine; y++) {
                    if (grille[x][y].composante != grille[0][0].composante) {
                        termine = false;
                    }
                }
            }

            if (termine) break;
        }


    }

    public String genererSVG() {
        int cellSize = 5; // Taille de chaque cellule en pixels
        int svgWidth = taille * cellSize + 1; // Nous retirons le "+ taille" ici
        int svgHeight = taille * cellSize + 1; // Nous retirons le "+ taille" ici

        StringBuilder svg = new StringBuilder();

        svg.append(String.format("<svg width='%d' height='%d' xmlns='http://www.w3.org/2000/svg'>\n", svgWidth, svgHeight));

        for (int i = 0; i < taille; i++) {
            for (int j = 0; j < taille; j++) {
                int x = j * cellSize;
                int y = i * cellSize;
                Cellule cell = grille[i][j];

                // Dessiner le mur du haut
                if (cell.murHaut) {
                    svg.append(String.format("<line x1='%d' y1='%d' x2='%d' y2='%d' stroke='black'/>\n", x, y, x + cellSize, y));
                }

                // Dessiner le mur de gauche
                if (cell.murGauche) {
                    svg.append(String.format("<line x1='%d' y1='%d' x2='%d' y2='%d' stroke='black'/>\n", x, y, x, y + cellSize));
                }

                // Dessiner le mur de droite
                if (cell.murDroit) {
                    svg.append(String.format("<line x1='%d' y1='%d' x2='%d' y2='%d' stroke='black'/>\n", x + cellSize, y, x + cellSize, y + cellSize));
                }

                // Dessiner le mur du bas
                if (cell.murBas) {
                    svg.append(String.format("<line x1='%d' y1='%d' x2='%d' y2='%d' stroke='black'/>\n", x, y + cellSize, x + cellSize, y + cellSize));
                }
            }
        }

        // Dessiner l'entrée en vert
        svg.append(String.format("<rect x='0' y='%d' width='%d' height='%d' fill='green'/>\n", entree * cellSize, cellSize, cellSize));

        // Dessiner la sortie en rouge
        svg.append(String.format("<rect x='%d' y='%d' width='%d' height='%d' fill='red'/>\n", (taille - 1) * cellSize, sortie * cellSize, cellSize, cellSize));

        svg.append("</svg>\n");
        return svg.toString();
    }




    public void sauvegarderSVG(String filename) throws IOException {
        String svgContent = genererSVG();
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filename))) {
            writer.write(svgContent);
        }
    }

    public List<Cellule> calculerPlusCourtChemin() {
        for (int i = 0; i < taille; i++) {
            for (int j = 0; j < taille; j++) {
                grille[i][j].coutActuel = Integer.MAX_VALUE;
                grille[i][j].precedente = null;
            }
        }

        PriorityQueue<Cellule> queue = new PriorityQueue<>(Comparator.comparingInt(cell -> cell.coutActuel));
        Cellule entreeCellule = getEntreeCellule();
        System.out.println();
        entreeCellule.coutActuel = 0;
        queue.add(entreeCellule);

        while (!queue.isEmpty()) {
            Cellule current = queue.poll();
            for (int[] voisin : getVoisins(current.x, current.y)) {
                int x = voisin[0];
                int y = voisin[1];
                Cellule voisinCellule = grille[x][y];
                boolean murPresent = (current.x == x && current.y > y && current.murGauche) ||
                        (current.x == x && current.y < y && current.murDroit) ||
                        (current.y == y && current.x > x && current.murHaut) ||
                        (current.y == y && current.x < x && current.murBas);

                if (!murPresent){
                    int nouveauCout = current.coutActuel + 1;
                    if (nouveauCout < voisinCellule.coutActuel) {
                        voisinCellule.coutActuel = nouveauCout;
                        voisinCellule.precedente = current;
                        queue.add(voisinCellule);
                    }

                }
            }
        }

        List<Cellule> chemin = new ArrayList<>();
        Cellule sortieCellule = grille[sortie][taille - 1];
        while (sortieCellule != null) {
            chemin.add(sortieCellule);
            sortieCellule = sortieCellule.precedente;
        }

        Collections.reverse(chemin);
        return chemin;
    }


    private List<int[]> getVoisins(int x, int y) {
        List<int[]> voisins = new ArrayList<>();

        if (x > 0) voisins.add(new int[]{x-1, y});
        if (x < taille - 1) voisins.add(new int[]{x+1, y});
        if (y > 0) voisins.add(new int[]{x, y-1});
        if (y < taille - 1) voisins.add(new int[]{x, y+1});

        return voisins;
    }



    public static void main(String[] args) throws IOException {
        int t = Integer.parseInt(args[0]);

        Labyrinthe lab = new Labyrinthe(t);
        lab.generer();
        lab.sauvegarderSVG("labyrinthe.svg");
    }

}

