#!/bin/bash

javac src/main/java/com/example/labyrinthgraphique/model/*.java
# Vérifier le nombre d'arguments
if [ "$#" -ne 1 ]; then
    echo "Un seul argument est requis."
    exit 1
fi

# Vérifier si l'argument est un nombre entier
if ! [[ $1 =~ ^[0-9]+$ ]]; then
    echo "L'argument doit être un nombre entier."
    exit 1
fi

java -cp src/main/java com.example.labyrinthgraphique.model.Labyrinthe "$1"

jar -cfe labyrinth.jar com.example.labyrinthgraphique.model.Labyrinthe -C ./target/classes/ .

# Exécuter le fichier JAR
java -jar labyrinth.jar "$1"



